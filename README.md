# 建立前後端分離專案敏捷開發流水線

## Lab整體流程架構圖

![k8sSummit2023.png](doc/lab.png)

## 主線任務

[1. Fork Github Repository ](doc/1.md)

[2. Github Repository配置](doc/2.md)

[3. Local開發環境設置](doc/3.md)

[4.首次進行部署與驗證](doc/4.md)

[5. 進行修改讓流水線自動部署吧](doc/5.md)



